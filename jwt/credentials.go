package jwt

import (
	"errors"
	"log"
	"net/http"
	"time"

	jwtGo "github.com/dgrijalva/jwt-go"
)

type credentials struct {
	CsrfString   string
	AuthToken    *jwtToken
	RefreshToken *jwtToken
	options      credentialsOptions
}

type credentialsOptions struct {
	AuthTokenValidTime    time.Duration
	RefreshTokenValidTime time.Duration
	CheckTokenID          TokenIDChecker
	SigningMethodString   string
	VerifyOnlyServer      bool
	Debug                 bool
}

func (c *credentials) myLog(stools interface{}) {
	if c.options.Debug {
		log.Println(info(stools))
	}
}

func (c *credentials) newTokenWithClaims(claims *ClaimsType, validTime time.Duration) *jwtToken {
	var newToken jwtToken

	newToken.Token = jwtGo.NewWithClaims(jwtGo.GetSigningMethod(c.options.SigningMethodString), claims)
	newToken.ParseErr = nil
	newToken.options.ValidTime = validTime
	newToken.options.SigningMethodString = c.options.SigningMethodString
	newToken.options.Debug = c.options.Debug

	return &newToken
}

func (c *credentials) updateAuthTokenFromRefreshToken() *jwtError {
	if c.RefreshToken == nil || c.RefreshToken.Token == nil {
		return newJwtError(errors.New("Refresh token is invalid. Cannot refresh auth token"), http.StatusUnauthorized)
	}

	refreshTokenClaims, ok := c.RefreshToken.Token.Claims.(*ClaimsType)
	if !ok {
		return newJwtError(errors.New("Cannot read token claims"), http.StatusInternalServerError)
	}

	// check if the refresh token has been revoked
	if c.options.CheckTokenID(refreshTokenClaims.StandardClaims.Id) {
		c.myLog("Refresh token has not been revoked")
		// has it expired?
		if c.RefreshToken.Token.Valid {
			c.myLog("Refresh token is not expired")
			// nope, the refresh token has not expired
			// issue a new tokens with a new csrf and update all expiries
			newCsrfString, err := generateNewCsrfString()
			if err != nil {
				return newJwtError(err, http.StatusInternalServerError)
			}

			c.CsrfString = newCsrfString

			err = c.AuthToken.updateTokenExpiryAndCsrf(newCsrfString)
			if err != nil {
				return newJwtError(err, http.StatusInternalServerError)
			}

			err = c.RefreshToken.updateTokenExpiryAndCsrf(newCsrfString)
			return err
		}

		c.myLog("Refresh token is invalid")
		return newJwtError(errors.New("Refresh token is invalid. Cannot refresh auth token"), http.StatusUnauthorized)
	}

	c.myLog("Refresh token has been revoked")
	return newJwtError(errors.New("Refresh token has been revoked. Cannot update auth token"), http.StatusUnauthorized)

}

func (c *credentials) validateCsrfStringAgainstCredentials() *jwtError {
	authTokenClaims, ok := c.AuthToken.Token.Claims.(*ClaimsType)
	if !ok {
		return newJwtError(errors.New("Cannot read token claims"), http.StatusInternalServerError)
	}

	if c.CsrfString != authTokenClaims.Csrf {
		return newJwtError(errors.New("CSRF token doesn't match value in jwts"), http.StatusUnauthorized)
	}

	return nil
}

func (c *credentials) validateAndUpdateCredentials() *jwtError {
	// first, check that the csrf token matches what's in the jwts
	if err := c.validateCsrfStringAgainstCredentials(); err != nil {
		return newJwtError(err, http.StatusInternalServerError)
	}

	// next, check the auth token in a stateless manner
	if c.AuthToken.Token.Valid {
		// auth token has not expired and is valid
		c.myLog("Auth token has not expired and is valid")
		return nil
	}
	if ve, ok := c.AuthToken.ParseErr.(*jwtGo.ValidationError); ok {
		c.myLog("Auth token is not valid")
		if ve.Errors&(jwtGo.ValidationErrorExpired) != 0 {
			c.myLog("Auth token is expired")
			if !c.options.VerifyOnlyServer {
				// attempt to update the tokens
				err := c.updateAuthTokenFromRefreshToken()
				return err
			}

			c.myLog("Auth token is expired and server is not authorized to issue new tokens")
			return newJwtError(errors.New("Auth token is expired and server is not authorized to issue new tokens"), http.StatusUnauthorized)
		}

		c.myLog("Error in auth token")
		return newJwtError(errors.New("Auth token is not valid, and not because it has expired"), http.StatusUnauthorized)
	}
	c.myLog("Error in auth token")
	return newJwtError(errors.New("Auth token is not valid, and not because it has expired"), http.StatusUnauthorized)

}

func (c *credentials) buildTokenWithClaimsFromString(tokenString string, verifyKey interface{}, validTime time.Duration) *jwtToken {
	// note @adam-hanna: should we be checking inputs? Especially the token string?
	var newToken jwtToken

	token, err := jwtGo.ParseWithClaims(tokenString, &ClaimsType{}, func(token *jwtGo.Token) (interface{}, error) {
		if token.Method != jwtGo.GetSigningMethod(c.options.SigningMethodString) {
			c.myLog("Incorrect singing method on token")
			return nil, errors.New("Incorrect singing method on token")
		}
		return verifyKey, nil
	})

	if token == nil {
		token = new(jwtGo.Token)
		token.Claims = new(ClaimsType)
		c.myLog("token is nil, set empty token (parse error=" + err.Error() + ")")
	}

	newToken.Token = token
	newToken.ParseErr = err

	newToken.options.ValidTime = validTime
	newToken.options.SigningMethodString = c.options.SigningMethodString
	newToken.options.Debug = c.options.Debug

	return &newToken
}
