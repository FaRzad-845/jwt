package jwt

import (
	"errors"
	"io/ioutil"
	"net/http"
	"time"

	jwtGo "github.com/dgrijalva/jwt-go"
)

const (
	defaultRefreshTokenValidTime  = 72 * time.Hour
	defaultAuthTokenValidTime     = 15 * time.Minute
	defaultBearerAuthTokenName    = "X-Auth-Token"
	defaultBearerRefreshTokenName = "X-Refresh-Token"
	defaultCSRFTokenName          = "X-CSRF-Token"
	defaultCookieAuthTokenName    = "AuthToken"
	defaultCookieRefreshTokenName = "RefreshToken"
)

func defaultTokenRevoker(tokenID string) error {
	return nil
}

func defaultErrorHandler(w http.ResponseWriter, r *http.Request) {
	errCode := r.Context().Value(contextErrCodeKey)
	statusCode := errCode.(int)
	switch statusCode {
	case http.StatusUnauthorized:
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	case http.StatusMethodNotAllowed:
		http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
		return
	default:
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}
}

func defaultCheckTokenID(tokenID string) bool {
	// return true if the token id is valid (has not been revoked). False for otherwise
	return true
}

// Options is a struct for specifying configuration options
type Options struct {
	SigningMethodString   string
	PrivateKeyLocation    string
	PublicKeyLocation     string
	HMACKey               []byte
	VerifyOnlyServer      bool
	BearerTokens          bool
	RefreshTokenValidTime time.Duration
	AuthTokenValidTime    time.Duration
	AuthTokenName         string
	RefreshTokenName      string
	CSRFTokenName         string
	Debug                 bool
	IsDevEnv              bool
}

func (o *Options) buildSignAndVerifyKeys() (signKey interface{}, verifyKey interface{}, err error) {
	if o.SigningMethodString == "HS256" || o.SigningMethodString == "HS384" || o.SigningMethodString == "HS512" {
		return o.buildHMACKeys()
	}

	if o.SigningMethodString == "RS256" || o.SigningMethodString == "RS384" || o.SigningMethodString == "RS512" {
		return o.buildRSAKeys()
	}

	if o.SigningMethodString == "ES256" || o.SigningMethodString == "ES384" || o.SigningMethodString == "ES512" {
		return o.buildESKeys()
	}

	err = errors.New("Signing method string not recognized")
	return
}

func (o *Options) buildHMACKeys() (signKey interface{}, verifyKey interface{}, err error) {
	if len(o.HMACKey) == 0 {
		err = errors.New("When using an HMAC-SHA signing method, please provide an HMACKey")
		return
	}

	if !o.VerifyOnlyServer {
		signKey = o.HMACKey
	}

	verifyKey = o.HMACKey

	return
}

func (o *Options) buildRSAKeys() (signKey interface{}, verifyKey interface{}, err error) {
	var signBytes []byte
	var verifyBytes []byte

	// check to make sure the provided options are valid
	if o.PrivateKeyLocation == "" && !o.VerifyOnlyServer {
		err = errors.New("Private key location is required")
		return
	}

	if o.PublicKeyLocation == "" {
		err = errors.New("Public key location is required")
		return
	}

	// read the key files
	if !o.VerifyOnlyServer {
		signBytes, err = ioutil.ReadFile(o.PrivateKeyLocation)
		if err != nil {
			return
		}

		signKey, err = jwtGo.ParseRSAPrivateKeyFromPEM(signBytes)
		if err != nil {
			return
		}
	}

	verifyBytes, err = ioutil.ReadFile(o.PublicKeyLocation)
	if err != nil {
		return
	}

	verifyKey, err = jwtGo.ParseRSAPublicKeyFromPEM(verifyBytes)
	if err != nil {
		return
	}

	return
}

func (o *Options) buildESKeys() (signKey interface{}, verifyKey interface{}, err error) {
	var signBytes []byte
	var verifyBytes []byte

	// check to make sure the provided options are valid
	if o.PrivateKeyLocation == "" && !o.VerifyOnlyServer {
		err = errors.New("Private key location is required")
		return
	}

	if o.PublicKeyLocation == "" {
		err = errors.New("Public key location is required")
		return
	}

	// read the key files
	if !o.VerifyOnlyServer {
		signBytes, err = ioutil.ReadFile(o.PrivateKeyLocation)
		if err != nil {
			return
		}

		signKey, err = jwtGo.ParseECPrivateKeyFromPEM(signBytes)
		if err != nil {
			return
		}
	}

	verifyBytes, err = ioutil.ReadFile(o.PublicKeyLocation)
	if err != nil {
		return
	}

	verifyKey, err = jwtGo.ParseECPublicKeyFromPEM(verifyBytes)
	if err != nil {
		return
	}

	return
}
